//
//  AppDelegate.h
//  MouseAlarm
//
//  Created by Masahiro Murase on 2014/03/03.
//  Copyright (c) 2014年 calmscape. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SettingsModel;
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong, readonly) SettingsModel *settingsModel;

@end
