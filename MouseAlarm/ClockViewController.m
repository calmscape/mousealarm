//
//  ViewController.m
//  MouseAlarm
//
//  Created by Masahiro Murase on 2014/03/03.
//  Copyright (c) 2014年 calmscape. All rights reserved.
//

#import "AppDelegate.h"
#import <konashi-ios-sdk/Konashi.h>
#import <FontAwesome+iOS/UIFont+FontAwesome.h>
#import <FontAwesome+iOS/NSString+FontAwesome.h>
#import "ClockViewController.h"
#import "MADigitalClockView.h"
#import "MAAnalogClockView.h"
#import "SettingsViewController.h"
#import "SettingsModel.h"
#import "MASoundAlarm.h"
#import "MATasteSaltyAlarm.h"
#import "MATasteSourAlarm.h"

@interface ClockViewController () <SettingsViewControllerDelegate, MAClockViewDelegate>
@property (nonatomic, strong) MAAlarm *alarmObject;
/// モデル情報
@property (nonatomic, weak) SettingsModel *settingsModel;

/// 現在表示中の時計ビュー
@property (nonatomic, strong) MAClockView *currentClockView;

@property (nonatomic, assign, getter = isForceAlarmModeEnabled) BOOL forceAlarmModeEnabled;

// Outets & Actions
@property (weak, nonatomic) IBOutlet UILabel *konashiReadyLabel;
@property (weak, nonatomic) IBOutlet UIButton *settingsButton;
@property (weak, nonatomic) IBOutlet UIButton *thunderButton;
@property (weak, nonatomic) IBOutlet UIButton *alarmButton;
- (IBAction)settingsButtonDidPush:(id)sender;
- (IBAction)alarmButtonDidPush:(id)sender;
- (IBAction)thunderButtonDidPush:(id)sender;
- (IBAction)thunderButtonDidRelease:(id)sender;

@end

@implementation ClockViewController

- (void)awakeFromNib
{
	[super awakeFromNib];

	_settingsModel = [(AppDelegate *)[UIApplication sharedApplication].delegate settingsModel];
	_forceAlarmModeEnabled = NO;

	[Konashi initialize];
	[Konashi addObserver:self selector:@selector(konashiDidReady) name:KONASHI_EVENT_READY];
}

-(void)dealloc
{
	[Konashi removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.

	// アイコンフォントをボタンに設定する
	UIFont *fontAwesome = [UIFont fontAwesomeFontOfSize:32];
  self.thunderButton.titleLabel.font = fontAwesome;
  [self.thunderButton setTitle:[NSString fontAwesomeIconStringForEnum:FAIconBolt] forState:UIControlStateNormal] ;
	
  self.settingsButton.titleLabel.font = fontAwesome;
  [self.settingsButton setTitle:[NSString fontAwesomeIconStringForEnum:FAIconCog] forState:UIControlStateNormal] ;
	
	self.alarmButton.titleLabel.font = fontAwesome;
  [self.alarmButton setTitle:[NSString fontAwesomeIconStringForEnum:FAIconBell] forState:UIControlStateNormal] ;
	
	[self configureKonashi];
  [self configureViews];
	self.alarmObject = [self createAlarm];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"showSettings"]) {
		// screen transition to SettingsViewController
		NSParameterAssert([[segue.destinationViewController topViewController] isKindOfClass:[SettingsViewController class]]);
		SettingsViewController *vc = (SettingsViewController *)[segue.destinationViewController topViewController];
		vc.delegate = self;
		vc.settings = self.settingsModel.settings;
	}
}

#pragma mark - private instance methods
- (MAAlarm *)createAlarm
{
	MAAlarm *alarm;
	TasteType tasteType = [self.settingsModel.settings[SMTasteTypeKey] intValue];
	switch (tasteType) {
		case TasteTypeSound:
			alarm = [[MASoundAlarm alloc] init];
			break;
		case TasteTypeSalty:
			alarm = [[MATasteSaltyAlarm alloc] init];
			break;
		case TasteTypeSour:
		default:
			alarm = [[MATasteSourAlarm alloc] init];
			break;
	}
	return alarm;
}

- (void)configureKonashi
{
	[Konashi findWithName:self.settingsModel.settings[SMKonashiIdentifierKey]];
}

- (void)configureViews
{
	// モデル情報をビューに反映する
	self.alarmButton.selected = [self.settingsModel.settings[SMAlarmEnabledKey] boolValue];
	[self changeClockView:[self.settingsModel.settings[SMAnalogClockEnabledKey] boolValue]];
	self.konashiReadyLabel.hidden = ![Konashi isReady];
}

/**
 表示する時計ビューを切り替える
 @param	analogClockEnabled	アナログ時計かデジタル時計か。
 */
- (void)changeClockView:(BOOL)analogClockEnabled
{
	MAClockView *view;
	if (analogClockEnabled) {
		if ([self.currentClockView isKindOfClass:[MAAnalogClockView class]]) {
			return;
		}
		CGRect rect = CGRectMake(0, 0, self.view.bounds.size.width * 0.8, self.view.bounds.size.height * 0.8);
		view = [[MAAnalogClockView alloc] initWithFrame:rect];
	}
	else {
		if ([self.currentClockView isKindOfClass:[MADigitalClockView class]]) {
			return;
		}
		CGRect rect = CGRectMake(0, 0, self.view.bounds.size.width * 0.8, self.view.bounds.size.height * 0.8);
		view = [[MADigitalClockView alloc] initWithFrame:rect];
	}
	[self.currentClockView removeFromSuperview];
	[self.view addSubview:view];
	[self.view sendSubviewToBack:view];
	view.center = CGPointMake(self.view.frame.size.width * 0.5, self.view.frame.size.height * 0.47);
	self.currentClockView = view;
	self.currentClockView.delegate = self;
	self.currentClockView.alarmTime = self.settingsModel.settings[SMAlarmTimeKey];
}

#pragma mark - Actions
- (IBAction)unwindActionForSegue:(UIStoryboardSegue *)segue
{
  // unwind segue
}

- (IBAction)settingsButtonDidPush:(id)sender
{
  [self performSegueWithIdentifier:@"showSettings" sender:self];
}

- (IBAction)thunderButtonDidPush:(id)sender
{
  // いつでも体感ボタン
	self.forceAlarmModeEnabled = YES;
	[self.alarmObject run];
}

- (IBAction)thunderButtonDidRelease:(id)sender
{
	[self.alarmObject stop];
	self.forceAlarmModeEnabled = NO;
}

- (IBAction)alarmButtonDidPush:(id)sender
{
	self.settingsModel.settings[SMAlarmEnabledKey] = @(![self.settingsModel.settings[SMAlarmEnabledKey] boolValue]);
	self.alarmButton.selected = [self.settingsModel.settings[SMAlarmEnabledKey] boolValue];
}

#pragma mark - SettingsViewControllerDelegate protocol method
- (void)settingsDidApply:(SettingsViewController*)controller
{
	self.settingsModel.settings = controller.settings;

	self.currentClockView.alarmTime = self.settingsModel.settings[SMAlarmTimeKey];
	[self configureViews];
	[self configureKonashi];
	self.alarmObject = [self createAlarm];
	
	[controller dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - MAClockViewDelegate protocol method
- (void)clockView:(MAClockView *)clockView didRangeAlarm:(BOOL)isRangeAlarm
{
	if (self.isForceAlarmModeEnabled) {
		//強制アラーム発動中
		return;
	}

	if (isRangeAlarm && [self.settingsModel.settings[SMAlarmEnabledKey] boolValue]) {
		[self.alarmObject run];
	}
	else {
		[self.alarmObject stop];
	}
}

#pragma mark - konashi event callback
- (void)konashiDidReady
{
	self.konashiReadyLabel.hidden = ![Konashi isReady];
	[Konashi pinMode:LED5 mode:OUTPUT];
	[Konashi digitalWrite:LED5 value:HIGH];
	[self.settingsModel.settings setObject:[Konashi peripheralName] forKey:SMKonashiIdentifierKey];
}
@end
