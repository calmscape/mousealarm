//
//  MAAlarm.h
//  MouseAlarm
//
//  Created by Masahiro Murase on 2014/03/07.
//  Copyright (c) 2014年 calmscape. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MAAlarm : NSObject

/**
 アラームを作動させる。
 サブクラスでオーバーライドする場合は、メソッドの最後で[super run]すること。
 */
- (void)run;

/**
 アラームを止める
 サブクラスでオーバーライドする場合は、メソッドの最後で[super run]すること。
 */
- (void)stop;

@property (nonatomic, assign, readonly, getter = isRunning) BOOL running;
@end
