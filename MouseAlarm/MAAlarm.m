//
//  MAAlarm.m
//  MouseAlarm
//
//  Created by Masahiro Murase on 2014/03/07.
//  Copyright (c) 2014年 calmscape. All rights reserved.
//

@import AVFoundation;
#import "MAAlarm.h"

@interface MAAlarm ()
@property (nonatomic, assign, readwrite) BOOL running;
@end

@implementation MAAlarm

- (id)init
{
	self = [super init];
	if (self) {
		_running = NO;
	}
	return self;
}

- (void)run
{
	self.running = YES;
}

- (void)stop
{
	self.running = NO;
}

@end
