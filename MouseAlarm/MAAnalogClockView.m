//
//  MAAnalogClockView.m
//  MouseAlarm
//
//  Created by calmscape on 2014/03/04.
//  Copyright (c) 2014年 calmscape. All rights reserved.
//

#import "MAAnalogClockView.h"

@interface MAAnalogClockView ()
@property (nonatomic, assign) NSInteger nowHour;
@property (nonatomic, assign) NSInteger nowMinute;
@property (nonatomic, assign) NSInteger nowSecond;
@end

@implementation MAAnalogClockView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
      self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
  // Drawing code
//  NSLog(@"%ld", (long)self.nowSecond);
  
  CGContextRef context = UIGraphicsGetCurrentContext();
  CGContextSetStrokeColorWithColor(context, [UIColor colorWithRed:0.0 green:122/255.0 blue:1.0 alpha:1.0].CGColor);
  CGContextSetFillColorWithColor(context, [UIColor colorWithRed:0.0 green:122/255.0 blue:1.0 alpha:1.0].CGColor);
  
  // draw dials
  for (int i = 0; i < 12; i++) {
    CGFloat circleWidth = 3.8;
    CGFloat x = (self.bounds.size.width / 2.0) + (self.bounds.size.width / 2.0) * sin(M_PI + (-2.0 * M_PI * i / 12.0)) * 0.9;
    CGFloat y = (self.bounds.size.height / 2.0) + (self.bounds.size.width / 2.0) * cos(M_PI + (-2.0 * M_PI * i / 12.0)) * 0.9;
    CGContextFillEllipseInRect(context, CGRectMake(x - circleWidth / 2.0, y - circleWidth / 2.0, circleWidth, circleWidth));  // 円を塗りつぶす
  }

  // draw second hand
  CGContextSetLineWidth(context, 0.5);
  CGFloat x = (self.bounds.size.width / 2.0) + (self.bounds.size.width / 2.0) * sin(M_PI + (-2.0 * M_PI * self.nowSecond / 60.0)) * 0.9;
  CGFloat y = (self.bounds.size.height / 2.0) + (self.bounds.size.width / 2.0) * cos(M_PI + (-2.0 * M_PI * self.nowSecond / 60.0)) * 0.9;
  CGContextMoveToPoint(context, self.bounds.size.width / 2.0, self.bounds.size.height / 2.0);
  CGContextAddLineToPoint(context, x, y);
  CGContextStrokePath(context);

  // draw minute hand
  CGContextSetLineWidth(context, 1.5);
  CGFloat minute = self.nowMinute + self.nowSecond / 60.0;
  x = (self.bounds.size.width / 2.0) + (self.bounds.size.width / 2.0) * sin(M_PI + (-2.0 * M_PI * minute / 60.0)) * 0.8;
  y = (self.bounds.size.height / 2.0) + (self.bounds.size.width / 2.0) * cos(M_PI + (-2.0 * M_PI * minute / 60.0)) * 0.8;
  CGContextMoveToPoint(context, self.bounds.size.width / 2.0, self.bounds.size.height / 2.0);
  CGContextAddLineToPoint(context, x, y);
  CGContextStrokePath(context);

  // draw hour hand
  CGFloat hour = self.nowHour + self.nowMinute / 60.0;
  x = (self.bounds.size.width / 2.0) + (self.bounds.size.width / 2.0) * sin(M_PI + (-2.0 * M_PI * hour / 12.0)) * 0.52;
  y = (self.bounds.size.height / 2.0) + (self.bounds.size.width / 2.0) * cos(M_PI + (-2.0 * M_PI * hour / 12.0)) * 0.52;
  CGContextMoveToPoint(context, self.bounds.size.width / 2.0, self.bounds.size.height / 2.0);
  CGContextAddLineToPoint(context, x, y);

  CGContextStrokePath(context);
}



- (void)displayLinkDidFire:(CADisplayLink *)sender
{
  unsigned unitFlags = NSHourCalendarUnit | NSMinuteCalendarUnit |  NSSecondCalendarUnit;
  NSDate *now = [NSDate date];
  NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:unitFlags fromDate:now];

  self.nowHour = dateComponents.hour;
  self.nowMinute = dateComponents.minute;
  self.nowSecond = dateComponents.second;
  
  [self setNeedsDisplay];

	[super displayLinkDidFire:sender];
}

@end
