//
//  MAClockView.h
//  MouseAlarm
//
//  Created by Masahiro Murase on 2014/03/03.
//  Copyright (c) 2014年 calmscape. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MAClockView;
@protocol MAClockViewDelegate <NSObject>
- (void)clockView:(MAClockView *)clockView didRangeAlarm:(BOOL)isRangeAlarm;
@end

@interface MAClockView : UIView

@property (nonatomic, weak) id<MAClockViewDelegate> delegate;

/**
 時計のロケール情報
 */
@property (nonatomic, strong) NSLocale *locale;

/**
アラーム時間
 */
@property (nonatomic, strong) NSDate *alarmTime;

- (void)displayLinkDidFire:(CADisplayLink *)sender;

@end
