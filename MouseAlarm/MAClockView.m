//
//  MAClockView.m
//  MouseAlarm
//
//  Created by Masahiro Murase on 2014/03/03.
//  Copyright (c) 2014年 calmscape. All rights reserved.
//

#import "MAClockView.h"

static const NSTimeInterval kAlarmRangeSecond = 60 * 3;

@interface MAClockView ()
{
	CADisplayLink *_displayLink;
}

@end

@implementation MAClockView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
		_displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(displayLinkDidFire:)];
		[_displayLink setFrameInterval:60];	//1sec
		[_displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
		
		_locale = [NSLocale currentLocale];
    }
    return self;
}

- (void)dealloc
{
	[_displayLink removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
}

#pragma mark - CADisplayLink callback
- (void)displayLinkDidFire:(CADisplayLink *)sender
{
	BOOL isArarmRange = [self isAlarmRangeNow];
	if ([self.delegate respondsToSelector:@selector(clockView:didRangeAlarm:)]) {
		[self.delegate clockView:self didRangeAlarm:isArarmRange];
	}
}

#pragma mark - private instance methods
/**
 現在時刻がアラーム時間内かどうかを判定する
 */
- (BOOL)isAlarmRangeNow
{
	//アラーム判定
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *dateComponents = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:[NSDate date]];
	NSDateComponents *alarmDateComponents = [gregorian components:(NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit) fromDate:self.alarmTime];
	
	NSDateComponents *comps = [[NSDateComponents alloc] init];
	[comps setYear:dateComponents.year];
	[comps setMonth:dateComponents.month];
	[comps setDay:dateComponents.day];
	[comps setHour:alarmDateComponents.hour];
	[comps setMinute:alarmDateComponents.minute];
	[comps setSecond:0];
	NSTimeInterval alarmTimeInterval = [[gregorian dateFromComponents:comps] timeIntervalSince1970];
	NSTimeInterval nowTimeInterval = [[NSDate date] timeIntervalSince1970];
	
	BOOL isArarmRange = NO;
	if ((alarmTimeInterval < nowTimeInterval) && (nowTimeInterval < (alarmTimeInterval + kAlarmRangeSecond))) {
		isArarmRange = YES;
	}
	return isArarmRange;
}

@end
