//
//  MADigitalClockView.m
//  MouseAlarm
//
//  Created by Masahiro Murase on 2014/03/03.
//  Copyright (c) 2014年 calmscape. All rights reserved.
//

#import "MADigitalClockView.h"

@interface MADigitalClockView ()
@end

@implementation MADigitalClockView



- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
		NSArray *containObjects = [[NSBundle mainBundle] loadNibNamed:@"MADigitalClockView"
																owner:self
															  options:nil];
		UIView *view = containObjects[0];
		view.frame = self.bounds;
		[self addSubview:view];
	}
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)displayLinkDidFire:(CADisplayLink *)sender
{
	UILabel *clockLabel = (UILabel *)[self viewWithTag:1];
	NSParameterAssert([clockLabel isKindOfClass:[UILabel class]]);

	NSDateFormatter *formatter = [NSDateFormatter new];
	formatter.locale = self.locale;
	[formatter setDateFormat:@"HH:mm:ss"];
	clockLabel.text = [formatter stringFromDate:[NSDate date]];

	[super displayLinkDidFire:sender];
}
@end
