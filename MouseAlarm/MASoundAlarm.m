//
//  MASoundAlarm.m
//  MouseAlarm
//
//  Created by Masahiro Murase on 2014/03/07.
//  Copyright (c) 2014年 calmscape. All rights reserved.
//

@import AVFoundation;
#import "MASoundAlarm.h"

@interface MASoundAlarm ()
@property (nonatomic, strong) AVAudioPlayer *player;
@end

@implementation MASoundAlarm
- (id)init
{
	self = [super init];
	if (self) {
		NSURL *soundURL = [NSURL fileURLWithPath:
											 [[NSBundle mainBundle]pathForResource:@"alert" ofType:@"aiff"]];
    _player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundURL error:nil];
    _player.numberOfLoops = -1;
    [_player prepareToPlay];
	}
	return self;
}

- (void)run
{
	if (!self.player.isPlaying) {
		self.player.currentTime = 0.0;
		[self.player play];
	}

	[super run];
}

- (void)stop
{
	if (self.player.isPlaying) {
		[self.player stop];
	}

	[super stop];
}

@end
