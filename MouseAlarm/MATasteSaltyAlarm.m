//
//  MATasteSaltyAlarm.m
//  MouseAlarm
//
//  Created by Masahiro Murase on 2014/03/07.
//  Copyright (c) 2014年 calmscape. All rights reserved.
//

#import <konashi-ios-sdk/Konashi.h>
#import "MATasteSaltyAlarm.h"

@interface MATasteSaltyAlarm ()
@property (nonatomic, assign) BOOL alarmSoundedOnce;
@end

@implementation MATasteSaltyAlarm

- (id)init
{
	self = [super init];
	if (self) {
		_alarmSoundedOnce = NO;
	}
	return self;
}

#pragma mark - public instance methods
-(void)run
{
	if ([Konashi isReady] && !self.isRunning) {
		[Konashi pwmMode:PIO5 mode:KONASHI_PWM_ENABLE];
		[Konashi pwmPeriod:PIO5 period:70000];
		[Konashi pwmDuty:PIO5 duty:484];
		self.alarmSoundedOnce = YES;
	}

	[super run];
}

- (void)stop
{
	if ([Konashi isReady] && self.isRunning) {
		[Konashi pwmMode:PIO5 mode:KONASHI_PWM_DISABLE];
		self.alarmSoundedOnce = NO;
	}

	[super stop];
}
@end
