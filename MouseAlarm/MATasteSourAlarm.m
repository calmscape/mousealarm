//
//  MATasteSourAlarm.m
//  MouseAlarm
//
//  Created by Masahiro Murase on 2014/03/09.
//  Copyright (c) 2014年 calmscape. All rights reserved.
//

#import <konashi-ios-sdk/Konashi.h>
#import "MATasteSourAlarm.h"

@interface MATasteSourAlarm ()
@property (nonatomic, assign) BOOL alarmSoundedOnce;
@end

@implementation MATasteSourAlarm

- (id)init
{
	self = [super init];
	if (self) {
		_alarmSoundedOnce = NO;
	}
	return self;
}

#pragma mark - public instance methods
-(void)run
{
	if ([Konashi isReady] && !self.isRunning) {
		[Konashi pinMode:LED2 mode:OUTPUT];
		[Konashi digitalWrite:LED2 value:HIGH];
		self.alarmSoundedOnce = YES;
	}
	
	[super run];
}

- (void)stop
{
	if ([Konashi isReady] && self.isRunning) {
		[Konashi digitalWrite:LED2 value:LOW];
		self.alarmSoundedOnce = NO;
	}
	
	[super stop];
}

@end
