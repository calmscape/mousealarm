//
//  SettingsModel.h
//  MouseAlarm
//
//  Created by Masahiro Murase on 2014/03/05.
//  Copyright (c) 2014年 calmscape. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(unsigned char, TasteType)
{
	TasteTypeSweet = 0,
	TasteTypeSour,
	TasteTypeSalty,
	TasteTypeBitter,
	TasteTypeSound,
};

extern NSString * const SMAnalogClockEnabledKey;
extern NSString * const SMAlarmEnabledKey;
extern NSString * const SMAlarmTimeKey;
extern NSString * const SMTasteTypeKey;
extern NSString * const SMKonashiIdentifierKey;

@interface SettingsModel : NSObject

/**
 Dictionary key-value
 @"analogClockEnabled" : BOOL
 @"alarmEnabled" : BOOL
 @"alarmTime" : NSDate
 @"tasteType" : TasteType
 @"konashiIdentifier" :NSString
 */
@property (nonatomic, strong) NSMutableDictionary *settings;

+ (NSArray *)tasteTypes;

@end
