//
//  SettingsModel.m
//  MouseAlarm
//
//  Created by Masahiro Murase on 2014/03/05.
//  Copyright (c) 2014年 calmscape. All rights reserved.
//

#import "SettingsModel.h"

NSString * const SMAnalogClockEnabledKey = @"analogClockEnabled";
NSString * const SMAlarmEnabledKey = @"alarmEnabled";
NSString * const SMAlarmTimeKey = @"alarmTime";
NSString * const SMTasteTypeKey = @"tasteType";
NSString * const SMKonashiIdentifierKey = @"konashiIdentifier";


@interface SettingsModel ()
@end

@implementation SettingsModel

- (id)init
{
	self = [super init];
	if (self) {

		_settings = [@{SMAnalogClockEnabledKey : @YES,
									SMAlarmEnabledKey : @NO,
									SMAlarmTimeKey : [NSDate date],
									SMTasteTypeKey : @(TasteTypeSound),
									SMKonashiIdentifierKey : [NSString string]
									} mutableCopy];
	}
	return self;
}

+ (NSArray *)tasteTypes
{
	return @[@"Sweet", @"Sour", @"Salty", @"Bitter", @"Sound"];
}

@end
