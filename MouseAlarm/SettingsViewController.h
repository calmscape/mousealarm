//
//  SettingsViewController.h
//  MouseAlarm
//
//  Created by Masahiro Murase on 2014/03/03.
//  Copyright (c) 2014年 calmscape. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SettingsModel.h"

@class SettingsViewController;
@protocol SettingsViewControllerDelegate <NSObject>
@required
- (void)settingsDidApply:(SettingsViewController*)controller;
@end

@interface SettingsViewController : UITableViewController
@property (nonatomic, weak) id<SettingsViewControllerDelegate> delegate;
@property (nonatomic, weak) NSMutableDictionary *settings;

@end
