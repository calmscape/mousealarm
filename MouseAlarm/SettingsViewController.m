//
//  SettingsViewController.m
//  MouseAlarm
//
//  Created by Masahiro Murase on 2014/03/03.
//  Copyright (c) 2014年 calmscape. All rights reserved.
//

#import "SettingsViewController.h"
#import "TasteSettingsViewController.h"

@interface SettingsViewController () <TasteSettingsViewControllerDelegate>
@property (nonatomic, strong) NSMutableDictionary *temporarySettings;
// Outlets & Actions
@property (weak, nonatomic) IBOutlet UIDatePicker *alarmDatePicker;
@property (weak, nonatomic) IBOutlet UISwitch *alarmEnabledSwich;
@property (weak, nonatomic) IBOutlet UISwitch *analogClockEnabledSwich;
@property (weak, nonatomic) IBOutlet UILabel *tasteLabel;
@property (weak, nonatomic) IBOutlet UITextField *konashiIdentifierTextField;
- (IBAction)pickerValueDidChange:(UIDatePicker *)sender;
- (IBAction)applyButtonDidPush:(id)sender;
- (IBAction)alarmEnableSwitchDidChange:(id)sender;
- (IBAction)konashiIdentifierEditingDidEnd:(id)sender;
@end

@implementation SettingsViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self) {
		// Custom initialization
	}
	return self;
	
}

- (void)setSettings:(NSDictionary *)settings
{
	_temporarySettings = [NSMutableDictionary dictionaryWithDictionary:settings];
}

- (NSDictionary *)settings
{
	return _temporarySettings;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	// Do any additional setup after loading the view.
	
	[self settingsToControlAttributes];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"tasteSelector"]) {
		[segue.destinationViewController setDelegate:self];
		[segue.destinationViewController setSettings:self.settings];
	}
	else {
		//unwindとみなす
	}
}

#pragma mark - private instance methods
/**
 設定情報をUIコントロールに反映する
 */
- (void)settingsToControlAttributes
{
	self.alarmEnabledSwich.on = [self.temporarySettings[SMAlarmEnabledKey] boolValue];
	[self alarmEnableSwitchDidChange:self.alarmEnabledSwich];

	self.analogClockEnabledSwich.on = [self.temporarySettings[SMAnalogClockEnabledKey] boolValue];
	
	self.tasteLabel.text = [[SettingsModel tasteTypes] objectAtIndex:[self.temporarySettings[SMTasteTypeKey] integerValue]];
	
	self.alarmDatePicker.date = self.temporarySettings[SMAlarmTimeKey];
	
	self.konashiIdentifierTextField.text = self.temporarySettings[SMKonashiIdentifierKey];
}

/**
 UIコントロールの状態を設定情報に反映する
 */
- (void)controlAttributesToSettings
{
	[self.temporarySettings setObject:@(self.alarmEnabledSwich.on) forKey:SMAlarmEnabledKey];
	[self.temporarySettings setObject:self.alarmDatePicker.date forKey:SMAlarmTimeKey];
	[self.temporarySettings setObject:@(self.analogClockEnabledSwich.on) forKey:SMAnalogClockEnabledKey];
	[self.temporarySettings setObject:self.konashiIdentifierTextField.text forKey:SMKonashiIdentifierKey];
}

#pragma mark - TasteSettingsViewControllerDelegate protocol methods
- (void)tasteSettingsDidApply:(TasteSettingsViewController *)controller tasteType:(TasteType)tasteType
{
	self.tasteLabel.text = [[SettingsModel tasteTypes] objectAtIndex:tasteType];

	
	//TODO: controlAttributesToSettingsに移す
	[self.temporarySettings setObject:@(tasteType) forKey:SMTasteTypeKey];

}

#pragma mark - Actions
- (IBAction)pickerValueDidChange:(UIDatePicker *)sender
{
	NSLog(@"%@", sender.date);
}

- (IBAction)applyButtonDidPush:(id)sender
{
	[self controlAttributesToSettings];
	[self.delegate settingsDidApply:self];
}

- (IBAction)alarmEnableSwitchDidChange:(UISwitch *)sender
{
	self.alarmDatePicker.userInteractionEnabled = sender.on;
	self.alarmDatePicker.alpha = sender.on ? 1.0 : 0.4;
}

- (IBAction)konashiIdentifierEditingDidEnd:(id)sender {
	[sender resignFirstResponder];
}
@end
