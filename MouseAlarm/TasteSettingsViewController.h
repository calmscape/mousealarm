//
//  TasteSettingsViewController.h
//  MouseAlarm
//
//  Created by Masahiro Murase on 2014/03/06.
//  Copyright (c) 2014年 calmscape. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SettingsModel.h"

@class TasteSettingsViewController;
@protocol TasteSettingsViewControllerDelegate <NSObject>
@required
- (void)tasteSettingsDidApply:(TasteSettingsViewController *)controller tasteType:(TasteType)tasteType;
@end

@interface TasteSettingsViewController : UITableViewController
@property (nonatomic, weak) NSMutableDictionary *settings;
@property (nonatomic, weak) id<TasteSettingsViewControllerDelegate> delegate;
@end
