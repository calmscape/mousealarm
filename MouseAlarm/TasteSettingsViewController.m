//
//  TasteSettingsViewController.m
//  MouseAlarm
//
//  Created by Masahiro Murase on 2014/03/06.
//  Copyright (c) 2014年 calmscape. All rights reserved.
//

#import "TasteSettingsViewController.h"

@interface TasteSettingsViewController ()
@end

@implementation TasteSettingsViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (id)initWithStyle:(UITableViewStyle)style
{
	self = [super initWithStyle:style];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	
	// Uncomment the following line to preserve selection between presentations.
	// self.clearsSelectionOnViewWillAppear = NO;


}

-(void) viewWillDisappear:(BOOL)animated {
	if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
		// back button was pressed.  We know this is true because self is no longer
		// in the navigation stack.
		if ([self.delegate respondsToSelector:@selector(tasteSettingsDidApply:tasteType:)]) {
			[self.delegate tasteSettingsDidApply:self tasteType:[self.settings[SMTasteTypeKey] charValue]];
		}
	}
	[super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}


#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[self.settings setObject:@(indexPath.row) forKey:SMTasteTypeKey];
	[tableView reloadSections:[NSIndexSet indexSetWithIndex:0]
					 withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	// Return the number of sections.
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	// Return the number of rows in the section.
	return [[SettingsModel tasteTypes] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *CellIdentifier = @"Cell";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
	
	// Configure the cell...
	if ([self.settings[SMTasteTypeKey] integerValue] == indexPath.row) {
		cell.accessoryType = UITableViewCellAccessoryCheckmark;
	}
	else {
		cell.accessoryType = UITableViewCellAccessoryNone;
		
	}
	cell.textLabel.text = [[SettingsModel tasteTypes] objectAtIndex:indexPath.row];
	return cell;
}
@end
