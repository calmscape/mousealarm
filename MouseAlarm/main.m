//
//  main.m
//  MouseAlarm
//
//  Created by Masahiro Murase on 2014/03/03.
//  Copyright (c) 2014年 calmscape. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
